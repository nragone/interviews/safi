import React from "react";

require("es6-promise").polyfill();
require("isomorphic-fetch");

import StaticsComponent from "@components/Statics/Statics";
import Loader from "@components/Loader";
import { Statics as StaticType } from "@pages/api/report/_postProcess";

import config from "@config";

import "./index.scss";
import "normalize.css";

const getStatics = async (page, file, filterByValue): Promise<StaticType> => {
  const statics: StaticType = await fetch(
    `${config.report}?page=${page ||
      1}&file=${file}&filterByValue=${filterByValue}`
  )
    .then(res => {
      return res.json();
    })
    .then(({ data }) => {
      return data;
    });

  return statics;
};

class HomePage extends React.Component {
  state = {
    statics: null,
    loading: true,
    err: false,
    fieldSelected: "Psum_kW"
  };

  componentDidMount() {
    this.updateStatics(1);
  }

  updateStatics = page => {
    this.setState({ statics: null, loading: true });
    getStatics(1, "demoCompressorWeekData.csv", this.state.fieldSelected)
      .then(statics => {
        this.setState({ statics: statics });
      })
      .catch(err => {
        this.setState({ error: err });
      })
      .finally(() => {
        this.setState({ loading: false });
      });
  };

  handleFieldChange = e => {
    this.setState({ fieldSelected: e.target.value }, () => {
      this.updateStatics(1);
    });
  };

  render() {
    const { statics, loading } = this.state;

    return (
      <div className="home">
        <h2>Data Reader</h2>
        <label htmlFor="metricid">MetricID: </label>
        <select
          name="metricid"
          onChange={this.handleFieldChange}
          value={this.state.fieldSelected}
        >
          <option value="Psum_kW">Psum_kW</option>
          <option value="Iavg_A">Iavg_A</option>
        </select>
        <hr />
        {loading && <Loader />}
        {!loading && statics ? <StaticsComponent statics={statics} /> : null}
        <hr />
      </div>
    );
  }
}

export default HomePage;
