import { NextApiRequest, NextApiResponse } from "next";
import parser from "@pages/api/report/_parser";
import postProcess, { Statics } from "@pages/api/report/_postProcess";

// Refresh rate in seconds
export const REFRESH_RATE = 30;
const ROWS_PER_LECTURE = 74;

const DEFAULT_COL_FILTERED = "metricid";

// Parameter that defines the operation load reference
const DEAFULT_FILTERED_COL_VALUE = "Psum_kW";

// Default page size set to 1 week
export const DEFAULT_PAGE_SIZE = (604800 / REFRESH_RATE) * ROWS_PER_LECTURE;

export type ReportResponse = {
  data: Statics;
};

/**
 * Handler for /api/report
 * This will feed the parser with the requested data
 */
export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { file, page, pageSize, filterByValue } = {
    file: "demoCompressorWeekData.csv",
    page: 1,
    pageSize: DEFAULT_PAGE_SIZE,
    filterByValue: DEAFULT_FILTERED_COL_VALUE,
    ...req.query
  };

  console.log(`------------------
/data/report file: ${file} pageSize: ${pageSize}
-------------------------`);

  console.time("parsingTime");
  const data = await parser(
    file,
    Number(page),
    Number(pageSize),
    DEFAULT_COL_FILTERED,
    filterByValue
  );

  console.log("Rows count: ", data.records.length);
  console.timeEnd("parsingTime");

  const statics: Statics = await postProcess(data.records);

  res.json({ data: statics });
};
