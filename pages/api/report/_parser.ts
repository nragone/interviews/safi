const fs = require("fs");
const csv = require("csv");

import { fileExist, createFile, jsonReader } from "@utils/files";

import { Data } from "@models/data";

const CACHE_PATH = "./cache";

/**
 * Parse and filter from csv file. Using https://csv.js.org for parsing
 * @param file csv file to parse
 * @param page page number to return
 * @param pageSize the size of each page
 * @param filterByCol column name to filter out from the file
 * @param filterByValue column value
 */
async function parseCSV(
  file: string,
  page: number,
  pageSize: number,
  filterByCol: string,
  filterByValue: string
): Promise<{ records: Data[] }> {
  let selectedData = [];

  // Page size fixed to 1 week.

  console.log(`-------------------------------
  PARSER file:${file} page:${page} pageSize:${pageSize}
  ----------------------------------------------`);

  const parserConfig = {
    columns: true,
    fromLine: page ? page * pageSize - pageSize + 1 : 1,
    to: pageSize,
    delimiter: ",",
    skipLinesWithError: true
  };

  console.log(`Filterging ${filterByCol}=${filterByValue}`);
  const cacheFileName = `${file.split(".")[0]}-${page}-${filterByValue}.json`;

  return new Promise((resolve, reject) => {
    const inCache = fileExist(`${CACHE_PATH}/${cacheFileName}`);

    if (inCache) {
      // Get data from cache
      const { data } = jsonReader(`${CACHE_PATH}/${cacheFileName}`);
      if (data) {
        return resolve({ records: data });
      }
    }

    const parser = csv.parse(parserConfig);

    parser.on("readable", () => {
      let row;
      while ((row = parser.read())) {
        if (row[filterByCol] === filterByValue) {
          selectedData.push(row);
        }
      }
    });

    parser.on("end", () => {
      createFile(CACHE_PATH, cacheFileName, selectedData);

      resolve({ records: selectedData });
    });

    parser.on("error", err => reject(err));

    const stream = fs.createReadStream(file);

    stream.on("readable", () => {
      let row;
      while ((row = stream.read())) {
        parser.write(row);
      }
    });

    stream.on("end", () => {
      stream.close();
      parser.end();
    });

    stream.on("error", err => reject(err));
  });
}

export default parseCSV;
