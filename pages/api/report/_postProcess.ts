import { Data, State } from "@models/data";
import { REFRESH_RATE } from "@pages/api/report";

type StateCondition = {
  name: State;
  condition: (value: number) => boolean;
};

let statesConditions = (minReference, maxReference): StateCondition[] => {
  return [
    {
      name: "off",
      condition: value => value === 0
    },
    {
      name: "onUnloaded",
      condition: value => value > 0 && value <= minReference
    },
    {
      name: "onIdle",
      condition: value => value < maxReference * 0.8
    },
    {
      name: "onLoaded",
      condition: value => value >= maxReference * 0.8
    }
  ];
};

export type Statics = {
  from: number;
  to: number;
  total: number;
  upTime: number;
  downTime: number;
  states: {
    counts: {
      [key: string]: number;
    };
    times: {
      [key: string]: number;
    };
  };
};

/**
 * Count the states based on conditions defineed above.
 * Calc downTime and upTime
 * @param records The rows to process
 * @returns states count and times
 */
export default async (records: Data[]): Promise<Statics> => {
  const statics: Statics = {
    from: Number(records[0].timestamp),
    to: Number(records[records.length - 1].timestamp),
    total: records.length,
    upTime: 0,
    downTime: 0,
    states: {
      counts: {
        off: 0,
        onUnloaded: 0,
        onIdle: 0,
        onLoaded: 0
      },
      times: {
        off: 0,
        onUnloaded: 0,
        onIdle: 0,
        onLoaded: 0
      }
    }
  };

  return new Promise((resolve, reject) => {
    console.time("minCalcTime");
    const minOperationLoad = Math.min(
      ...records.map(record => record.recvalue)
    );
    console.timeEnd("minCalcTime");

    console.time("maxCalcTime");
    const maxOperationLoad = Math.max(
      ...records.map(record => record.recvalue)
    );
    console.timeEnd("maxCalcTime");

    const conditions = statesConditions(minOperationLoad, maxOperationLoad);

    records.reduce((acc, record) => {
      const state: StateCondition = conditions.find(state =>
        state.condition(record.recvalue)
      );

      if (state.name === "off" || state.name === "onUnloaded") {
        statics.downTime = statics.downTime + REFRESH_RATE;
      } else {
        statics.upTime = statics.upTime + REFRESH_RATE;
      }

      acc.counts[state.name] = acc.counts[state.name] + 1;
      acc.times[state.name] = acc.times[state.name] + REFRESH_RATE;

      return acc;
    }, statics.states);

    resolve(statics);
  });
};
