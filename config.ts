const domain = process.env.domain || "http://localhost:3000";
const apiUrl = `${domain}/api`;

export default {
  apiUrl,
  report: `${apiUrl}/report`
};
