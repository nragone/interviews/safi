# Safi Interview

# About the Problem

Our users want to know when a machine is on or off, loaded or unloaded. That will help them understand a) how utilized their equipment is, b) how long it takes to change tools, c) if the equipment is under-loaded, then how to improve the efficiency.

The 4 states are: Off, On - unloaded, On - idle, On - loaded.

- Off is power off, no data.
- On - unloaded is power on, no current (or essentially no current, it could be 0.1A or something).
- On - idle is less than 20% of operating load.
- On - loaded is 20-100+% of operating load.

Note that this is a compressor. It cycles between being fully loaded and being partially loaded as the pressure in the tank rises and falls from the target pressure.

## The challenge:

Build a simple web interface that tracks and displays uptime vs downtime for this machine for a week. The interface should be written in whatever stack you want (node/vue/express isn't a requirement).

## The data:

This is real but modified data from a real machine. It comes with all the warts and irregularities of real data.

# About the Solution

Base skeleton with React, NextJS, Typescript and Sass cloned from https://github.com/nragone/react-next-now

For Development

```
npm install
npm run dev
```

For production

```
npm run build
npm start
```

A local server should start at http://localhost:3000

# Explanation

When a user lands on the home page will automatically request a report corresponding to 1 week counting from the first date registered in the CSV file. Should be possible to request subsequent weeks using a page parameter, but that is not tested, the records on the csv file are actually not enough to fill one week.
Also, the default metridic for the first report is Psum_kW, you can change this using the select field and it will automatically generate a new report.

## Behind the scenes

**pages/api/report/index**

The system takes the parameters: file(not used), page, pageSize, filterByValue and use those to feed a parser. Also, this file contains the default values for those parameters.

**pages/api/report/\_parser**

The parser is using a library (https://csv.js.org) to parse the csv file but only the rows corresponding to the parameters requested and ignoring the lines with errors.
After this first process we have a reduced set of rows each represented as an object now.

**pages/api/report/\_postProcess**

Iterating over the reduced set of data we get the max and the min value for the field selected, whit these values we can now count occurrences based on the conditions defined for each state.

# Things to improve

- Add upload file capabilities

- After the first request, we can save the results to a json file for example, in this way the next request can read from that file reducing significative the response time (basically a cache implementation). I think that this approach is important for this kind of things that require a lot of processing and are generating always the same response (for the same window of time of course).

- Add a calendar to show the data on it. With this, the user could see the days on which event happened and the time on that day.
  My first approach was to show only numbers, which I think is useful for the client, but a calendar will be also a good option to add.

- Unit Test :S
