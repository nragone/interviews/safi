const fs = require("fs");

export function fileExist(filePath) {
  try {
    fs.accessSync(filePath, fs.constants.R_OK);
    return true;
  } catch (err) {
    return false;
  }
}

export async function removeFile(filePath) {
  return new Promise((resolve, reject) => {
    fs.unlink(filePath, err => {
      if (err) {
        console.error(err);
        return reject(err);
      }

      resolve();
    });
  });
}

export function createFile(path, fileName, data) {
  const filePath = `${path}/${fileName}`;

  // If cache path doesn't exists create it
  if (!fs.existsSync(path)) {
    fs.mkdirSync(path);
  }

  // Delete file if exists

  try {
    fs.writeFileSync(filePath, JSON.stringify({ data }), "utf8", {
      flag: "w"
    });

    return true;
  } catch (err) {
    return false;
  }
}

export function jsonReader(filePath) {
  try {
    const data = fs.readFileSync(filePath);
    try {
      return JSON.parse(data);
    } catch (err) {
      return null;
    }
    return data;
  } catch (err) {
    return null;
  }
}
