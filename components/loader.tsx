import { CSSProperties } from "react";

const style: { [key: string]: CSSProperties } = {
  container: {
    position: "absolute",
    width: "100%",
    background: "rgba(0,0,0,0.5)",
    color: "white",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  }
};

export default () => {
  return (
    <div style={style.container}>
      <div>Loading...</div>
    </div>
  );
};
