import moment from "moment";
import { ReportResponse } from "@pages/api/report";
import dynamic from "next/dynamic";

import "./styles.scss";

const PieChart = dynamic(() => import("@components/PieChart"), { ssr: false });

const styles = {
  states: {
    off: {
      backgroundColor: "#0088FE"
    },
    onUnloaded: {
      backgroundColor: "#FF8042"
    },
    onIdle: {
      backgroundColor: "#FFBB28"
    },
    onLoaded: {
      backgroundColor: "#00C49F"
    }
  }
};

const humanize = seconds => {
  return moment.duration(seconds, "seconds").humanize();
};

const StaticsComponent = ({ statics }: { statics: ReportResponse["data"] }) => {
  console.log(statics);
  const {
    states: { counts, times },
    from,
    to,
    upTime,
    downTime,
    total
  } = statics;

  const fromDate = new Date(from);
  const toDate = new Date(to);

  const parsedForPieChart = [
    { name: "OFF ", value: Math.round((counts.off / total) * 100) },
    {
      name: "UNLOADED",
      value: Math.round((counts.onUnloaded / total) * 100)
    },
    {
      name: "IDLE",
      value: Math.round((counts.onIdle / total) * 100)
    },
    {
      name: "LOADED",
      value: Math.round((counts.onLoaded / total) * 100)
    }
  ];

  const RenderState = ({ name, label, value }) => {
    return (
      <div className="state" style={{ ...styles.states[name] }}>
        {label}: {value}
      </div>
    );
  };

  return (
    <div className="statics-component">
      <h3>Statics</h3>
      <div className="container">
        <div className="header">
          <div>
            <div>
              <span>✅ </span>Up Time: {humanize(upTime)}
            </div>
            <hr />
            <div>
              <span>⚠️ </span>Down Time: {humanize(downTime)}
            </div>
          </div>
          <div>
            <div>Total Records: {total}</div>
            <hr />
            <div>From: {fromDate.toLocaleDateString()}</div>
            <div>To: {toDate.toLocaleDateString()}</div>
          </div>
        </div>
        <div className="chart-container">
          <div>
            <PieChart data={parsedForPieChart} />
          </div>
          <div>
            <div className="states-container">
              <RenderState
                name="off"
                label="OFF"
                value={times.off && humanize(times.off)}
              />
              <RenderState
                name="onUnloaded"
                label="UNLOADED"
                value={times.onUnloaded && humanize(times.onUnloaded)}
              />
              <RenderState
                name="onIdle"
                label="IDLE"
                value={times.onIdle && humanize(times.onIdle)}
              />
              <RenderState
                name="onLoaded"
                label="LOADED"
                value={times.onLoaded && humanize(times.onLoaded)}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default StaticsComponent;
