import React from "react";
import { PieChart, Pie, Cell } from "recharts";

const COLORS = ["#0088FE", "#FF8042", "#FFBB28", "#00C49F"];

type Props = {
  data: { name: string; value: number }[];
};

export default ({ data }: Props) => {
  return (
    <PieChart width={250} height={250}>
      <Pie
        data={data}
        cx={125}
        cy={100}
        outerRadius={80}
        fill="#8884d8"
        dataKey="value"
      >
        {data.map((entry, index) => (
          <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
        ))}
      </Pie>
    </PieChart>
  );
};
