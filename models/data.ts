// Data model from CSV file
//timestamp, metricid, deviceid, recvalue, calcvalue, excthreshold, excthlimit, deviation
//1534723200000, PF3, demo_ca1_t_axm, 2.77, 2.77, f, f, 0
export type Data = {
  timestamp: number;
  metricid: string;
  deviceid: string;
  recvalue: number;
  calcvalue: number;
  excthreshold: string;
  excthlimit: string;
  deviation: number;
};

export type State = "off" | "onUnloaded" | "onIdle" | "onLoaded";
